<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Note;
use App\Form\NoteType;
use App\Repository\NoteRepository;
use App\Service\FileUploader;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class NoteController extends AbstractController
{
    #[Route('/', name: 'note_index', methods: ['GET'])]
    public function index(Request $request, NoteRepository $noteRepository, $page = 1): Response
    {
        $page = (int)$request->query->get('page');
        $maxResult = 10;
        $notes = $noteRepository->getByPage($page,$maxResult);
        $totalNotes = count($noteRepository->findAll());


        $view = $this->render('note/index.html.twig', [
            'notes' => $notes,
            'page' => $page,
            'maxResult' => $maxResult,
            'total' => $totalNotes,

        ]);
        if ($request->isXmlHttpRequest() ) {
            $response = new JsonResponse();
            $dataResponse['view'] = $view->getContent();
            $response->setData($dataResponse);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return $view;
    }


    #[Route('/new', name: 'note_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            if ($note->getDocuments() != null) {
                foreach ($note->getDocuments() as $document){
                    $docFileName = $fileUploader->upload($document->getFile());
                    $directory = $fileUploader->getTargetDirectory();
                    $fullPath = $directory.'/'.$docFileName;
                    $document->setPath($fullPath);
                    $document->setName($docFileName);
                    $document->setNote($note);
                    $note->addDocument($document);
                    $entityManager->persist($document);
                }
            }

            $entityManager->persist($note);
            $entityManager->flush();

            return $this->redirectToRoute('note_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('note/new.html.twig', [
            'note' => $note,
            'form' => $form,
        ]);
    }

    #[Route('/show/{id}', name: 'note_show', methods: ['GET'])]
    public function show(Note $note): Response
    {
        return $this->render('note/show.html.twig', [
            'note' => $note,
        ]);
    }

    #[Route('/{id}/edit', name: 'note_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Note $note, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(NoteType::class, $note);
        $originalDocuments = new ArrayCollection();
        foreach ($note->getDocuments() as $originalDoc) {
            $originalDocuments->add($originalDoc);
        }
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalDocuments as $originalDoc) {
                if (false === $note->getDocuments()->contains($originalDoc)) {
                    $note->getDocuments()->removeElement($originalDoc);
                    $entityManager->remove($originalDoc);
                    if(file_exists($originalDoc->getPath())) unlink($originalDoc->getPath());
                }
            }

            foreach ($note->getDocuments() as $document){
                if($document->getFile() != null){
                    $docFileName = $fileUploader->upload($document->getFile());
                    $directory = $fileUploader->getTargetDirectory();
                    $fullPath = $directory.'/'.$docFileName;
                    $document->setPath($fullPath);
                    $document->setName($docFileName);
                    $document->setNote($note);
                    $note->addDocument($document);
                    $entityManager->persist($document);
                }
            }
            $entityManager->persist($note);
            $entityManager->flush();

            return $this->redirectToRoute('note_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('note/edit.html.twig', [
            'note' => $note,
            'form' => $form,
        ]);
    }


    #[Route('/{id}', name: 'note_delete', methods: ['POST'])]
    public function delete(Request $request, Note $note): Response
    {
        if ($this->isCsrfTokenValid('delete'.$note->getId(), (string) $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            if(count($note->getDocuments()) > 0 ){
                foreach ($note->getDocuments() as $doc) {
                    if(file_exists($doc->getPath())) unlink($doc->getPath());
                }
            }
            $entityManager->remove($note);
            $entityManager->flush();
        }

        return $this->redirectToRoute('note_index', [], Response::HTTP_SEE_OTHER);
    }

}
