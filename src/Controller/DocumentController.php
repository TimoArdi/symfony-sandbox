<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Note;
use App\Form\NoteType;
use App\Repository\NoteRepository;
use App\Service\FileUploader;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/document')]
class DocumentController extends AbstractController
{
    #[Route('/download/{id}', name: 'document_download')]

    public function downloadDocument($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $doc = $em->getRepository(Document::class)->find($id);
        $response = new Response();
        $response->setContent(file_get_contents($doc->getPath()));
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-disposition', 'filename='.$doc->__toString());

        return $response;
    }

}
